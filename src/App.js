import React from 'react'
import BeerForm from './components/BeerForm'
import BeerList from './components/BeerList'
import VisibilityFilter from './components/VisibilityFilter'
import NavBar from './components/NavBar'
import Beer from './components/Beer'
import Notification from './components/Notification'
import beerService from './services/beers'
import loginService from './services/login'
import PendingBeerList from './components/PendingBeerList'
import pendingBeerService from './services/pendingBeers'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      beers: [],
      pendingBeers: [],
      currentUser: null
    }
  }

  filterSelected = (value) => {
    console.log(value)
  }

  logout() {
    loginService.logout();
  }

  componentDidMount() {
    beerService.getAll().then(beers =>
      this.setState({ beers })
    )
    pendingBeerService.getAll().then(pendingBeers => 
      this.setState({ pendingBeers })
    )
    loginService.currentUser().then(currentUser => 
      this.setState({ currentUser })
    )

    let asd = loginService.currentUser()
  }

/*   componentWillMount() {
    beerService
      .getAll()
      .then(beers => {
        this.setState({ beers })
      })
  } */

  render() {
    return (
      <div>
        <NavBar />
        {/* <BeerForm />
        <VisibilityFilter /> */}
        <BeerList beers={this.state.beers}/>
        {this.state.currentUser !== null &&
          <PendingBeerList beers={this.state.pendingBeers}/>
        }
      </div>
    )
  }
}

export default App