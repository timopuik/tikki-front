import axios from 'axios'
const baseUrl = '/api/login'

const login = async (credentials) => {
    const response = await axios.post(baseUrl, credentials)
        .then(user => {
            localStorage.setItem('currentUser', JSON.stringify(user));
        })
    return response.data
}

const currentUser = async () => {
    let currentti = JSON.parse(localStorage.getItem('currentUser'));
    return currentti;
}

const logout = async () => {
    localStorage.removeItem('currentUser');
}

export default { login, currentUser, logout }