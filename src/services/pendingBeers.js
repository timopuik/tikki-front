import axios from 'axios'
const url = 'http://localhost:3001/api/pendingBeers'

const getAll = async () => {
  const response = await axios.get(url)
  return response.data
}

const getOne = async (id) => {
  let suffix = `${id}`
  let newurl = `${url}/${id}`
  const response = await axios.get(newurl)
  console.log('loytyyko sourcea: ', response.data)
  return response.data
}

const createBeer = async (beerData) => {
  console.log('create beer: ', beerData)
  const response = await axios.post(url, { beerData })
  return response.data
}

export default {
    getAll, getOne, createBeer
  }