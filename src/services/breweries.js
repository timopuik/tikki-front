import axios from 'axios'
// const baseUrl = 'http://localhost:3001/beers'
const url = 'http://localhost:3001/api/breweries'

const getAll = async () => {
    const response = await axios.get(url)
    return response.data
  }
  
  const createNew = async (content) => {
    const response = await axios.post(url, { content })
    return response.data
  }
  
  export default {
    getAll, createNew
  }