import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import beerReducer from './reducers/beerReducer'
import filterReducer from './reducers/filterReducer'

const reducer = combineReducers({
  beers: beerReducer,
  filter: filterReducer
})

const store = createStore(
  reducer,
  applyMiddleware(thunk)
)

export default store