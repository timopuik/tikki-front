import beerService from '../services/beers'

const beerReducer = (state = [], action) => {
  console.log('ACTION: ', action)
  switch (action.type) {
  case 'NEW_BEER':
    return [...state, action.data]
  case 'TOGGLE_IMPORTANCE': {
    const id = action.data.id
    const beerToChange = state.find(n => n.id === id)
    const changedBeer = { ...beerToChange, important: !beerToChange.important }
    return state.map(beer => beer.id !== id ? beer : changedBeer )
  }
  case 'INIT_BEERS':
    return action.data
  default:
    return state
  }
}

export const initializeBeers = () => {
  return async (dispatch) => {
    const beers = await beerService.getAll()
    dispatch({
      type: 'INIT_BEERS',
      data: beers
    })
  }
}

export const createNew = (content) => {
  return async (dispatch) => {
    const newBeer = await beerService.createNew(content)
    dispatch({
      type: 'NEW_BEER',
      data: newBeer
    })
  }
}

export const toggleImportance = (beer) => {
  return async (dispatch) => {
    const changedBeer = {
      content: beer.content,
      importance: !beer.importance
    }

    await beerService.createNew(beer.id, changedBeer)
    dispatch({
      type: 'TOGGLE_IMPORTANCE',
      data: { id: beer.id },
    })
  }
}

export default beerReducer