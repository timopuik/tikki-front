import React from 'react'
import BeerForm from './components/BeerForm'
import BeerList from './components/BeerList'
import VisibilityFilter from './components/VisibilityFilter'
import Beer from './components/Beer'
import Notification from './components/Notification'
import beerService from './services/beers'
import loginService from './services/login'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      beers: [],
      newBeer: '',
      showAll: true,
      error: null,
      username: '',
      password: '',
      user: null
    }
  }

  componentDidMount() {
    beerService.getAll().then(beers =>
    this.setState({ beers })
    )

    const loggedUserJSON = window.localStorage.getItem('loggedTikkiUser')
    if (loggedUserJSON) {
      const user = JSON.parse(loggedUserJSON)
      this.setState({user})
      beerService.setToken(user.token)
    }
  }

  componentWillMount() {
    beerService
      .getAll()
      .then(beers => {
        this.setState({ beers })
      })
  }

  toggleVisible = () => {
    this.setState({ showAll: !this.state.showAll })
  }

  addBeer = (event) => {
    event.preventDefault()
    const beerObject = {
      content: this.state.newBeer,
      date: new Date(),
      important: Math.random() > 0.5
    }

    beerService
      .create(beerObject)
      .then(newBeer => {
        this.setState({
          beers: this.state.beers.concat(newBeer),
          newBeer: ''
        })
      })
  }

  toggleImportanceOf = (id) => {
    return () => {
      const beer = this.state.beers.find(b => b.id === id)
      const changedBeer = { ...beer, important: !beer.important }

      beerService
        .update(id, changedBeer)
        .then(changedBeer => {
          this.setState({
            beers: this.state.beers.map(beer => beer.id !== id ? beer : changedBeer)
          })
        })
        .catch(error => {
          this.setState({
            error: `olut '${beer.content}' on jo valitettavasti poistettu palvelimelta`,
            beers: this.state.beers.filter(b => b.id !== id)
          })
          setTimeout(() => {
            this.setState({ error: null })
          }, 50000)
        })
    }
  }

  login = async (event) => {
    event.preventDefault()
    console.log('logging in with', this.state.username, this.state.password)
    try {
      const user = await loginService.login({
        username: this.state.username,
        password: this.state.password
      })

      window.localStorage.setItem('loggedTikkiUser', JSON.stringify(user))

      beerService.setToken(user.token)

      this.setState({ username: '', password: '', user })
    } catch (exception) {
      this.setState({
        error: 'käyttäjätunnus tai salasana virheellinen',
      })
      setTimeout(() => {
        this.setState({ error: null })
      }, 5000)
    }
  }

  handleBeerChange = (event) => {
    console.log(event.target.value)
    this.setState({ newBeer: event.target.value })
  }

  handleLoginFieldChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
  }

  render() {
    const beersToShow =
      this.state.showAll ?
        this.state.beers :
        this.state.beers.filter(beer => beer.important === true)

    const label = this.state.showAll ? 'vain tärkeät' : 'kaikki'

    const loginForm = () => (
      <div>
        <h2>Kirjaudu</h2>

        <form onSubmit={this.login}>
          <div>
            käyttäjätunnus
            <input
              type="text"
              name="username"
              value={this.state.username}
              onChange={this.handleLoginFieldChange}
            />
          </div>
          <div>
            salasana
            <input
              type="password"
              name="password"
              value={this.state.password}
              onChange={this.handleLoginFieldChange}
            />
          </div>
          <button>kirjaudu</button>
        </form>
      </div>
    )

    const beerForm = () => (
      <div>
        <h2>Syötä uusi olut</h2>

        <form onSubmit={this.addBeer}>
          <input
            value={this.state.newBeer}
            onChange={this.handleBeerChange}
          />
          <button>tallenna</button>
        </form>
      </div>
    )


    return (
      <div>
        <h1>Oluet</h1>

        <Notification message={this.state.error} />

        {this.state.user === null ?
          loginForm() :
          <div>
            <p>{this.state.user.name} logged in</p>
            {beerForm()}
          </div>
        }

        <div>
          <button onClick={this.toggleVisible}>
            näytä {label}
          </button>
        </div>
        <ul>
          {beersToShow.map(beer => 
            <Beer 
              key={beer.id} 
              beer={beer} 
            />)}
        </ul>

      </div>
    )
  }
}

export default App