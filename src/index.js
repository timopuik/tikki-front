import ReactDOM from 'react-dom'
import React from 'react'
import App from './App'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Login from './components/Login'
import PendingBeer from './components/PendingBeer'

// WORKING OLD VERSION WITHOUT LOGIN ROUTE
//ReactDOM.render(
//    <App />,
//  document.getElementById('root')
//)

ReactDOM.render(
  <Router>
    <div>
      <Route exact path="/" component={App} />
      <Route path="/login" component={Login} />
      <Route path='/pendingBeer/:id' component={PendingBeer} />
    </div>
  </Router>,
  document.getElementById('root')
)