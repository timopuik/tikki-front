import React, { Component } from 'react'
import Beer from './Beer'
import { Grid, Paper, Typography } from "@material-ui/core"
import Card from "@material-ui/core/Card"
import CardActionArea from "@material-ui/core/CardActionArea"
import CardContent from "@material-ui/core/CardContent"


class BeerList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      biiru: this.props.beers
    }
    const biiru = this.props.beers
  }


  render() {


    return (
      <div style={{ marginTop: 20, padding: 30}}>
        <Grid container spacing={40} justify="center">
          <h1>Kaliat</h1>
          {this.props.beers.map(beer =>
            <Grid item key={beer.name}>
              <Card>
                <CardActionArea>
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                      {beer.name}
                    </Typography>
                    <Typography component="p">{beer.id}</Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          )}
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
    return {
      beers: state.beers,
      filter: state.filter
    }
  }

export default BeerList