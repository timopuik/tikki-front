import React from 'react'
//import Brewery from './components/Brewery'
import breweriesService from '../services/breweries'
import NavBar from '../components/NavBar'
import pendingBeersService from '../services/pendingBeers'
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import PropTypes from 'prop-types'

/* PendingBeer.propTypes = {
  name: PropTypes.string.isRequired
}
*/

class Popup extends React.Component {
  render() {
    return (
      <div className='popup'>
        <div className='popup_inner'>
          <h2>{this.props.text}</h2>
          <Formik
            initialValues={{
            breweryName: ''
        }}
        validationSchema={Yup.object().shape({
          breweryName: Yup.string().required('Brewery name is required')
        })}
        onSubmit={({ breweryName }, { setStatus, setSubmitting }) => {
          let breweryData = { breweryName: breweryName };
          breweriesService.createNew(breweryData)
            .then(
              resp => {
                // update breweries list in PendingBeer component
                this.props.update()
              },
              error => {
                setSubmitting(false);
                this.setStatus(error);
              }
            );
        }}
        render={({ errors, status, touched, isSubmitting }) => (
          <Form>
            <div className="form-group">
              <label htmlFor="breweryName">Brewery name</label>
              <Field name="breweryName" type="text" className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} />
              <ErrorMessage name="name" component="div" className="invalid-feedback" />
            </div>
            <div classname="form-group">
              <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button>
            </div>
          </Form>
        )}
        />
        <button onClick={this.props.closePopup}>Close</button>
        </div>
      </div>
    );
  }
}

class PendingBeer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      breweries: [],
      pendingBeer: null,
      pendingBeerId: null,
      source: null,
      showPopup: false,
      selected: null
    };

    // see if logged in user is found
    //let currentti = loginService.currentUser
    // if (currentti === isNullOrUndefined) {
    //   this.props.history.push('/');
    // }
  }

  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  }

  componentDidMount() {
    breweriesService.getAll()
      .then(breweries => this.setState({ breweries }))

    let { id } = this.props.match.params
    this.setState({ pendingBeerId: id })

    pendingBeersService.getOne(id)
      .then(pendingBeer => this.setState({ pendingBeer: pendingBeer.name, source: 'Alko' })
    )
  }

  handleChange(event) {
    console.log('event is... ', event)
    this.setState({ selected: event.target.value });
  }

  updateBreweries() {
    breweriesService.getAll()
      .then(breweries => this.setState({ breweries }))
  }

  changeSelected(value) {
    console.log('value is... ', value)
    this.setState({ selected: value })
  }

  render() {
    const breweryName = '';
    const password = '';

    let optionItems = this.state.breweries.map((brewery) =>
      <option key={brewery.name}>{brewery.name}</option>
    );

    console.log('printing pendingbeer props', this.props)
    console.log('printing state: ', this.state.pendingBeer)
    console.log('printing pendingbeerid... ', this.state.pendingBeerId)

    return (
      <div>
        <NavBar />
        <h2>Pending beer</h2>
        <button onClick={this.togglePopup.bind(this)}>Add brewery</button>
        {this.state.showPopup ? 
          <Popup
            text='Add brewery'
            closePopup={this.togglePopup.bind(this)}
            update={this.updateBreweries.bind(this)}
          />
          : null
        }
        <label>Brewery:</label>
        <select
          name="brewery"
          onChange={(event) => this.changeSelected(event.target.value)}
          value={this.state.selected}
        >
          {optionItems}
        </select>
        <Formik
        initialValues={{
          breweryName: '',
          name: ''
        }}
        validationSchema={Yup.object().shape({
          name: Yup.string().required('Beer name is required')
        })}
        onSubmit={({ name }, { setStatus, setSubmitting }) => {
          let beerData = { breweryName: this.state.selected, beerName: name, pendingBeerId: this.state.pendingBeerId };
          pendingBeersService.createBeer(beerData)
            .then(
              resp => {
                // TODO: use this to subsequently create beerInstance 
              },
              error => {
                setSubmitting(false);
                this.setStatus(error);
              }
            );
        }}
        render={({ errors, status, touched, isSubmitting }) => (
          <Form>
            <div className="form-group">
              <label>Source</label>
              <textarea  value={this.state.source} readonly="readonly" />
              <br /><label>SourceFullName</label>
              <textarea value={this.state.pendingBeer} readonly="readonly" />
            </div>  
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <Field name="name" type="text" className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} />
              <ErrorMessage name="name" component="div" className="invalid-feedback" />
            </div>
            <div classname="form-group">
              <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Submit</button>
            </div>
          </Form>
        )}
        />
      </div>
    )
  }
}

export default PendingBeer