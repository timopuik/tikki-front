import React from 'react'
import PropTypes from 'prop-types'

const Beer = ( beer ) => {
  return (
    // <li onClick={handleClick}>
    <li>
      <p>{beer.name}</p>
      <p>{beer.style}</p>
    </li>
  )
}

Beer.propTypes = {
  name: PropTypes.string.isRequired
}

export default Beer