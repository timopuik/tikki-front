import React, { useState } from 'react'
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import loginService from '../services/login'
import { isNullOrUndefined } from 'util';

class Login extends React.Component {
  constructor(props) {
    super(props);

    // see if logged in user is found
    let currentti = loginService.currentUser
    // if (currentti === isNullOrUndefined) {
    //   this.props.history.push('/');
    // }
  }

  render() {
    const username = '';
    const password = '';

    return (
      <div>
      <h2>login</h2>
      <Formik
        initialValues={{
          username: '',
          password: ''
        }}
        validationSchema={Yup.object().shape({
          username: Yup.string().required('Username is required'),
          password: Yup.string().required('Password is required')
        })}
        onSubmit={({ username, password }, { setStatus, setSubmitting }) => {
          // setStatus();
          let credentials = { username: username, password: password };
          loginService.login(credentials)
            .then(
              user => {
                // onko tää väärin?
                localStorage.setItem('currentUser', JSON.stringify(user));
                const { from } = this.props.location.state;
                // const { from } = this.props.location.state || { from: { pathname: "/" } };
                this.props.history.push(from);
              },
              error => {
                setSubmitting(false);
                this.setStatus(error);
              }
            );
        }}
        render={({ errors, status, touched, isSubmitting }) => (
          <Form>
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <Field name="username" type="text" className={'form-control' + (errors.username && touched.username ? ' is-invalid' : '')} />
              <ErrorMessage name="username" component="div" className="invalid-feedback" />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
              <ErrorMessage name="password" component="div" className="invalid-feedback" />
            </div>
            <div classname="form-group">
              <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Login</button>
            </div>
          </Form>
        )}
      />  
    </div>
    )
  }

/*   onSubmit = async (event) => {
    event.preventDefault()
    try {
      const user = await loginService.login({
        username, password
      })
    } catch (e) {
      return false;
    }
  } */
}

const LoginFunc = ({ onLogin, history }) => {
    const onSubmit = (event) => {
      event.preventDefault()
      history.push('/')
    }
    return (
      <div>
        <h2>login</h2>
        <form onSubmit={onSubmit}>
          <input
            label="username"
          />
          <input
            label="password"
            type='password'
          />
          <button type='submit'>login</button>
        </form>   
      </div>
    )
}

export default Login