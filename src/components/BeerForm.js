import React from 'react'
import { TextField } from '@material-ui/core';
import { Button } from '@material-ui/core'

class BeerForm extends React.Component {

  addBeer = async (event) => {
    var content = {
      name: event.target.beer.value,
      brewery: event.target.brewery.value,
      style: event.target.style.value
    }
    event.preventDefault()
    const name = event.target.beer.value
    event.target.beer.value = ''
    this.props.createNew(content)
  }

  handleChange = e => {

  }

  render() {
    return (
      <form onSubmit={this.addBeer}>
        <TextField label="Name" name="beer" variant="outlined" /><br />
        <TextField label="Brewery" name="brewery" variant="outlined" /><br />
        <TextField label="Style" name="style" variant="outlined" /><br />
        <Button
          label="lisää"
          variant="raised"
          primary={true}
          type="submit"
          />
      </form>
    )
  }
}

export default BeerForm