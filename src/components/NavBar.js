import React from 'react'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Button } from '@material-ui/core';
import loginService from '../services/login'

const styles = {
    root: {
      flexGrow: 1,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
  };

const NavBar = () => {
    
  function logout() {
    loginService.logout();
    }

    return(
        <div>
        <AppBar position="static">
            <Toolbar>
                <Typography variant="title" color="inherit">
                Tikki
                </Typography>
            </Toolbar>
            <Button color="inherit">Login</Button>
            <Link to="/login">LOGIN</Link>
            <a onClick={logout}>Logout</a>
            <Link to="/pending">Pending</Link>
        </AppBar>
        </div>
    )
}

export default withStyles(styles)(NavBar);